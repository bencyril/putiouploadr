# Putio-Uploadr

After searching for something that could help facilitate transfering files from local/apps/services to Putio, nothing corresponded to what I was looking for.
Almost all projects were at least 2years old.

So I decided to do something simple in Go, Putio-Uploadr

## Table of Contents

* [Installation](#installation)
* [Configuration](#configuration)
* [Advanced Usage](#advanced-usage)
  * [Docker](#docker)
  * [Docker-compose](#docker-compose)
* [Example](#example)

## Installation

Just build the image with the given Dockerfile:

    docker build --no-cache -t putio-uploadr .

## Configuration

To make it run, you need to set 5 ENV variables:

| Key | Content |
| ------- | ------- |
| PUTIO_TOKEN | Token to communicate with Putio APIs |
| PUTIO_WATCH_FOLDER | Folder to watch for new files |
| PUTIO_DOWNLOAD_FOLDER_ID | ID of the folder in PUTIO where you want to upload the file, in general it's 0 but could be something else |
| LOG_LEVEL | Log level you want stdout to show |
| PROMETHEUS_PORT | Which port will be use to expose Prometheus metrics |

To know the DOWNLOAD_FOLDER_ID, just go to your Putio account a chose the folder where you want your file to bbe uploaded
In the URL, you should see something like: `https://app.put.io/files/your_folder_id`

## Advanced Usage

## LOG & PROMETHEUS
LOGS and Prometheus metrics have been added to the new version of Putio Uploadr
It will allows to have better json'ed logs with mor informations and metrics related to torrent/magnet upload

It's customizable via 2 environment variables:
 - LOG_LEVEL
 - PROMETHEUS_PORT

You can use Info and Debug right now, maybe more will be available in new releases
For Prometheus, everything is configured to have metrics on the /metrics endpoints, only the port where the app will expose them is customizable.

### Docker

```
docker create \
  --name=putio-uploadr \
  -e PUID=1000 \
  -e PGID=1000 \
  -e PUTIO_TOKEN=xxx \
  -e PUTIO_WATCH_FOLDER=/torrents \
  -e PUTIO_DOWNLOAD_FOLDER_ID=0 \
  -e LOG_LEVEL=info \
  -e PROMETHEUS_PORT=8280 \
  -v /path/to/torrent:/torrents \
  --restart unless-stopped \
  putio-uploadr
```

### Docker-compose

```
---
version: "3.7"
services:
  putio:
    image: putio-uploadr
    container_name: putio-uploadr
    environment:
      - PUID=1000
      - PGID=1000
      - PUTIO_TOKEN=xxx
      - PUTIO_WATCH_FOLDER=/torrents
      - PUTIO_DOWNLOAD_FOLDER_ID=0
      - LOG_LEVEL=info
      - PROMETHEUS_PORT=8280
    volumes:
      - /path/to/torrent:/torrents
    restart: unless-stopped
```

### Example
![alt text](https://i.imgur.com/xzM3Srr.png "Example of logs given by PutioUploadr")
