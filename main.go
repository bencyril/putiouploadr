package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

  "golang.org/x/oauth2"
  "github.com/putdotio/go-putio"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"github.com/radovskyb/watcher"
	log "github.com/sirupsen/logrus"
)

func init() {
	// Log as JSON instead of the default ASCII formatter.
	log.SetFormatter(&log.JSONFormatter{})

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	log.SetOutput(os.Stdout)

	// Only log the warning severity or above.
	logLvl, err := log.ParseLevel(os.Getenv("LOG_LEVEL"))
	if err != nil {
		log.WithFields(standardFields).Error("doesn't understand log level")
	}
	log.SetLevel(logLvl)
}

var (
	folderPath       string = os.Getenv("PUTIO_WATCH_FOLDER")
	putioToken       string = os.Getenv("PUTIO_TOKEN")
	downloadFolderID string = os.Getenv("PUTIO_DOWNLOAD_FOLDER_ID")
	torrentType      string = "torrent"
	magnetType       string = "magnet"
	prometheusPort   string = os.Getenv("PROMETHEUS_PORT")

	standardFields = log.Fields{
		"hostname": os.Getenv("HOSTNAME"),
		"appname":  "putioUploadr",
	}

	torrentProcessed = promauto.NewCounter(prometheus.CounterOpts{
		Name: "radarr_nbr_torrent_dl",
		Help: "The total number of torrent downloaded",
	})

	magnetProcessed = promauto.NewCounter(prometheus.CounterOpts{
		Name: "radarr_nbr_magnet_dl",
		Help: "The total number of magnet transfered",
	})
)

func connectToPutio() *putio.Client {
	tokenSource := oauth2.StaticTokenSource(&oauth2.Token{AccessToken: putioToken})
	oauthClient := oauth2.NewClient(context.Background(), tokenSource)

	client := putio.NewClient(oauthClient)

	return client
}

func runScript() ([]byte, error) {
	// Running the bash script that will rename files by replacing space by dot
	// Argument is where to rename file, here is the watch folder where files will be DL
	cmd := exec.Command("/bin/ash", "/chFileName.sh", folderPath)
	// cmd.Env = []string{"A=B"}
	output, err := cmd.CombinedOutput()
	if err != nil {
		return nil, errors.New(string(output))
	}
	return output, nil
}

func folderIDConvert(fodlerID string) (int64, error) {
	folderID, err := strconv.ParseInt(downloadFolderID, 10, 32)
	if err != nil {
		err := fmt.Errorf("strconv err: %v", err)
		return 0, err
	}
	return folderID, nil
}

func uploadToPutio(filename string, path string, client *putio.Client) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(time.Second*5))
	defer cancel()

	// Convert FolderID from string to int to use with Files.Upload
	folderID, err := folderIDConvert(downloadFolderID)
	if err != nil {
		return err
	}

	// Using open since Upload need an *os.File variable
	//filePath := filepath.Join(folderPath, filepath.Clean(path))
	file, err := os.Open(folderPath + "/" + filename)
	//file, err := os.Open(filePath)
	if err != nil {
		err := fmt.Errorf("openfile err: %v", err)
		return err
	}

	// Uploading file to Putio
	result, err := client.Files.Upload(ctx, file, filename, folderID)
	if err != nil {
		err := fmt.Errorf("upload to Putio err: %v", err)
		return err
	}

	msg := fmt.Sprintf("File: %v has been uploaded to Putio at %v", filename, result.Transfer.CreatedAt)
	log.WithFields(standardFields).Info(msg)
	// Increment nbr torrent upload counter for Prometheus
	torrentProcessed.Inc()
	return nil
}

func transferToPutio(filename string, path string, client *putio.Client) error {
	contextLogger := log.WithFields(standardFields)

	// Creating a context with 5 second timout in case Transfer is too long
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(time.Second*5))
	defer cancel()

	// Convert FolderID from string to int to use with Files.Upload
	folderID, err := folderIDConvert(downloadFolderID)
	if err != nil {
		return err
	}

	// Reading the link inside the magnet file to give to Putio
	magnetData, err := ioutil.ReadFile(folderPath + "/" + filename)
	if err != nil {
		err := fmt.Errorf("couldn't read file %v: %v", filename, err)
		return err
	}

	// Using Transfer to DL file via magnet file
	result, err := client.Transfers.Add(ctx, string(magnetData), folderID, "")
	if err != nil {
		err := fmt.Errorf("transfer to putio err: %v", err)
		return err
	}

	msg := fmt.Sprintf("File: %v has been transfered to Putio at %v", filename, result.CreatedAt)
	contextLogger.Info(msg)
	// Increment nbr magnet trasnfered counter for Prometheus
	magnetProcessed.Inc()
	return nil

}

func checkFileType(filename string) string {
	// Checking what's at the end of the string
	isMagnet := strings.HasSuffix(filename, ".magnet")
	isTorrent := strings.HasSuffix(filename, ".torrent")

	if isMagnet {
		return "magnet"
	} else if isTorrent {
		return "torrent"
	} else {
		return ""
	}
}

func cleaningFilename(fileName string) string {
	// Retrieve the events and convert it into string to eb able to work with it
	// wordsWithSpace := fmt.Sprintf("%v", event)

	// Convert the string by replacing the space by dot
	cleanFileName := strings.Replace(fileName, " ", ".", -1)
	// Regex to only take what's inside the double quote
	// re2 := regexp.MustCompile(`"(.*?)"`)
	// cleanFileNameRegex := re2.FindStringSubmatch(wordsWithSpace)
	// // We only take the second result as it's the good one
	// // fmt.Println("Not totally finish filename: ", cleanFileNameRegex)
	// cleanFileName := cleanFileNameRegex[1]

	return cleanFileName
}

func prepareFile(event watcher.Event, client *putio.Client) {
	var filepath string
	var err error
	var fileType string

	contextLogger := log.WithFields(standardFields)

	// We run the script to **rename the file** by repalcing space by dot
	_, err = runScript()
	if err != nil {
		contextLogger.Error("couldn't run the script: ", err)
	}

	// From events with lots of informations
	// To a clean filename ready to be used
	cleanFileName := cleaningFilename(event.Name())
	// rename file from whitespace to dot
	// os.Rename(event.Path, cleanFileName)

	// Checking if the file is a torrent of a magnet file
	fileType = checkFileType(cleanFileName)
	if fileType == torrentType || fileType == magnetType {
		msg := fmt.Sprintf("File: %v has been added to the folder", cleanFileName)
		contextLogger.Info(msg)

		if fileType == torrentType {
			err = uploadToPutio(cleanFileName, filepath, client)
			if err != nil {
				contextLogger.Error("error uploading file to Putio: ", err)
			}
		} else if fileType == magnetType {
			err = transferToPutio(cleanFileName, filepath, client)
			if err != nil {
				contextLogger.Error("error transfering magnet to Putio: ", err)
			}
		} else {
			contextLogger.Error("error, file is not torrent or magnet: ", err)
		}
	} else {
		msg := fmt.Sprintf("filetype is not torrent or magnet, it's: %v", fileType)
		contextLogger.WithField("filename", cleanFileName).Error(msg)
		return
	}
}

func watchFolder(client *putio.Client, out1 chan error) {
	w := watcher.New()

	// SetMaxEvents to 1 to allow at most 1 event's to be received
	// on the Event channel per watching cycle.
	//
	// If SetMaxEvents is not set, the default is to send all events.
	w.SetMaxEvents(1)

	// Only notify rename and move events.
	w.FilterOps(watcher.Move, watcher.Create)

	// Only files that match the regular expression during file listings
	// will be watched.
	// r := regexp.MustCompile("^abc$")
	// w.AddFilterHook(watcher.RegexFilterHook(r, false))

	go func() {
		for {
			select {
			case event := <-w.Event:
				prepareFile(event, client) // Print the event's info.
			case err := <-w.Error:
				log.Fatalln(err)
			case <-w.Closed:
				return
			}
		}
	}()

	// Watch this folder for changes.
	if err := w.Add(folderPath); err != nil {
		out1 <- fmt.Errorf("couldn't watch folder: %v", err)
		// return fmt.Errorf("couldn't watch folder: %v", err)
		return
	}

	// Start the watching process - it'll check for changes every 100ms.
	if err := w.Start(time.Millisecond * 100); err != nil {
		out1 <- fmt.Errorf("couldn't watch folder: %v", err)
		// return fmt.Errorf("couldn't start watching folder: %v", err)
		return
	}
	return
}

func checkEnvVariables() error {
	var envToSet string

	if folderPath == "" {
		envToSet = "PUTIO_WATCH_FOLDER / "
	}
	if downloadFolderID == "" {
		envToSet = envToSet + "PUTIO_DOWNLOAD_FOLDER_ID / "
	}
	if putioToken == "" {
		envToSet = envToSet + "PUTIO_TOKEN / "
	}
	// If envToSet isn't empty it means some variables are missing
	if envToSet != "" {
		return errors.New(envToSet)
	}

	return nil
}

// Healthcheck is used by Kube to see if pod is ready
func healthcheck(w http.ResponseWriter, r *http.Request) {
	// No need to pollute Stdout with kube healthcheck
	log.WithFields(standardFields).WithFields(log.Fields{
		"url":  r.URL.Path,
		"host": r.Host,
	}).Debug("OK")

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")

	resp := make(map[string]string)
	resp["msg"] = "OK"
	jsonResp, err := json.Marshal(resp)
	if err != nil {
		log.WithFields(standardFields).Error("error happened in JSON marshal. err: %s", err)
	}
	w.Write(jsonResp)
}

func main() {
	log.Println("putioUploadr Started")

	// Create a connection to Putio services
	client := connectToPutio()
	log.WithFields(standardFields).Info("connected to Putio")

	// We check that the env variable are set to avoid issues
	err := checkEnvVariables()
	if err != nil {
		log.WithFields(standardFields).Error("missing variables: ", err)
		os.Exit(1)
	}

	// Allows to have the server accepting request while in the background
	// the watcher check the folder for new torrent/magnet
	go func() {
		out1 := make(chan error)
		watchFolder(client, out1)
		err = <-out1
		if err != nil {
			log.WithFields(standardFields).Error("error: ", err)
			os.Exit(1)
		}
	}()
	// To be able to use the http server and the folder watcher
	// the http server needs to be before the watcher to be able to function
	//Create the default mux
	mux := http.NewServeMux()

	mux.Handle("/metrics", promhttp.Handler())
	mux.HandleFunc("/healthcheck", healthcheck)

	//Create the server.
	s := &http.Server{
		Addr:    ":" + prometheusPort,
		Handler: mux,
	}
	log.WithFields(standardFields).Info("server is runing on port %v", prometheusPort)

	err = s.ListenAndServe()
	if err != nil {
		log.WithFields(standardFields).Error("error with running listenAndServe	: ", err)
		os.Exit(1)
	}
}
