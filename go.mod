module gitlab.com/putioUploadr

go 1.16

require (
	cloud.google.com/go v0.65.0 // indirect
	github.com/igungor/go-putio v0.0.0-20170802205209-94406739e2bd
	github.com/prometheus/client_golang v1.11.0 // indirect
	github.com/putdotio/go-putio v1.7.1 // indirect
	github.com/radovskyb/watcher v1.0.7
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/oauth2 v0.2.0
)
