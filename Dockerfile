FROM golang:alpine as builder

WORKDIR /app

# Copy and download now to put them in cache
# next build, it will direct go build instead of 
# retrieving all modules
COPY go.mod /app
COPY go.sum /app
RUN go mod download

COPY . /app

RUN go build -o putioUploadr


FROM alpine

COPY --from=builder /app/putioUploadr /
COPY script/chFileName.sh /chFileName.sh

# Default port on which app will expose 
# Prometheus metrics
EXPOSE 8280

CMD [ "/putioUploadr" ]
